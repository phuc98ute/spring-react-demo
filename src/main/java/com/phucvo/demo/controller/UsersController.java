package com.phucvo.demo.controller;

import com.phucvo.demo.entity.User;
import com.phucvo.demo.links.UserLinks;
import com.phucvo.demo.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by : phucvo
 * Date       : 8/7/21
 * Time       : 4:27 PM
 * Package    : com.phucvo.demo.controller
 * Filename   : UsersController
 */

@Slf4j
@RestController
@RequestMapping("/api/")
public class UsersController {
    @Autowired
    UsersService usersService;

    @GetMapping(path = UserLinks.LIST_USERS)
    public ResponseEntity<?> listUsers() {
        log.info("UserController: list users");
        List<User> resource = usersService.getUsers();
        return ResponseEntity.ok(resource);
    }

    @PostMapping(path = UserLinks.ADD_USERS)
    public ResponseEntity<?> saveUser(@RequestBody User user){
        log.info("UserController: add user");
        User resource = usersService.saveUser(user);
        return ResponseEntity.ok(resource);
    }
}
