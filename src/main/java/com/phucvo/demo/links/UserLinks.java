package com.phucvo.demo.links;

import org.springframework.stereotype.Component;

/**
 * Created by : phucvo
 * Date       : 8/7/21
 * Time       : 4:38 PM
 * Package    : com.phucvo.demo.links
 * Filename   : UserLinks
 */
@Component
public class UserLinks {
    public static final String LIST_USERS = "/users";
    public static final String ADD_USERS = "/user";
}
