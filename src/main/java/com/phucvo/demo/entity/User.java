package com.phucvo.demo.entity;

import javax.validation.constraints.NotNull;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by : phucvo
 * Date       : 8/7/21
 * Time       : 4:30 PM
 * Package    : com.phucvo.demo.entity
 * Filename   : User
 */
@Entity
@Data
public class User {
    @Id
    @Column
    private long id;

    @Column
    @NotNull(message = "{NotNull.User.firstName}")
    private String firstName;

    @Column
    @NotNull(message = "{NotNull.User.lastName}")
    private String lastName;

    @Column
    @NotNull(message = "{NotNull.User.email")
    private String email;
}
