package com.phucvo.demo.repository;

import com.phucvo.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by : phucvo
 * Date       : 8/7/21
 * Time       : 4:33 PM
 * Package    : com.phucvo.demo.repository
 * Filename   : UsersReposity
 */
@RepositoryRestResource
public interface UsersReposity extends JpaRepository<User,Integer>, JpaSpecificationExecutor<User>, QuerydslPredicateExecutor<User> {
}
