package com.phucvo.demo.service;

import com.phucvo.demo.entity.User;
import com.phucvo.demo.repository.UsersReposity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * Created by : phucvo
 * Date       : 8/7/21
 * Time       : 4:29 PM
 * Package    : com.phucvo.demo.service
 * Filename   : UsersService
 */
@Service
public class UsersService {
    private UsersReposity usersReposity;

    public UsersService(UsersReposity usersReposity) {
        this.usersReposity = usersReposity;
    }

    public List<User> getUsers() {
        return usersReposity.findAll();
    }

    public User saveUser(User user){
        user.setId(new Random().nextInt());
        return usersReposity.save(user);
    }
}
